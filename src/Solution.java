import java.util.ArrayList;
import java.util.List;

/**
 * This class adds groups from arrays to return a new array with the numbers
 * added.
 *
 * @author Leonardo Herrera
 */
public class Solution {

    /**
     * This method adds groups from array.
     *
     * <p>
     *     For this, it makes use of several loops to go through the array
     *     and add the even and odd groups that exist, save them in a list
     *     and then create another array to pass the new numbers.
     * </p>
     *
     * @param arr Is a param of type array.
     * @return Returns a value of type int, is the new array size.
     */
    public static int sumGroups(int[] arr) {
        List<Integer> auxList = new ArrayList<>();
        int sumOfIndex = 0;

        if(arr.length == 1) {
            auxList.add(arr[0]);
        } else {
            for (int i = 0; i < arr.length - 1; i++) {
                if(sumOfIndex == 0) {
                    sumOfIndex += arr[i];
                }
                if(arr[i] % 2 == arr[i + 1] % 2) {
                    sumOfIndex += arr[i + 1];

                } else {
                    auxList.add(sumOfIndex);
                    sumOfIndex = 0;
                }

                if(i == arr.length - 2) {
                    if(arr[arr.length - 2] % 2 != arr[arr.length - 1] % 2) {
                        auxList.add(arr[i + 1]);
                    } else {
                        auxList.add(sumOfIndex);
                        sumOfIndex = 0;
                    }
                }
            }
        }

        int[] finalArray = new int[auxList.size()];

        for (int j = 0; j < auxList.size(); j++) {
            finalArray[j] = auxList.get(j);
        }

        return finalArray.length;
    }

    /*public static int updateArray(int[] array) {
        boolean voucher = false;
        int[] result = arrayUpdate(array);
        while (!voucher) {
            sumGroups(result);
            if(arrayUpdate(result) == result) {
                voucher = true;
            }
        }
        return result.length;
    }*/
}
